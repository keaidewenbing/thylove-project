package cn.thylove.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: dsq
 * @data: 2023/11/25 12:10
 * @description: user
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
  private String username;

  private String passwd;
}
