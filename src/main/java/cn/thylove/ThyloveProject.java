package cn.thylove;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThyloveProject {

	public static void main(String[] args) {
		SpringApplication.run(ThyloveProject.class, args);
	}

}
