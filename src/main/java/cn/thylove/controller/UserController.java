package cn.thylove.controller;

import cn.thylove.entity.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: dsq
 * @data: 2023/11/25 12:08
 * @description: UserController
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @GetMapping()
    public User getUser() {
        return new User("dsq", "123456");
    }
}
